#This file is part of Tryton. The COPYRIGHT file at the top level of
#this repository contains the full copyright notices and license terms.
import copy
import datetime
from decimal import Decimal
from trytond.model import ModelView, ModelSQL, fields
from trytond.wizard import Wizard
from trytond.modules.company import CompanyReport
from trytond.pyson import Equal, Eval, Or, Not, Get, Bool
from trytond.transaction import Transaction
from trytond.pool import Pool


class Pricelist(ModelSQL, ModelView):
    _name = 'pricelist.pricelist'

    def __init__(self):
        super(Pricelist, self).__init__()
        new_sel = ('special', 'Special Offer')
        self.type = copy.copy(self.type)
        if new_sel not in self.type.selection:
            self.type.selection.append(new_sel)
            self._reset_columns()

        self._error_messages.update({
            'only_one_default': 'There can be only one active Default Pricelist '
                    'by type and company!\n\n'
                    'There can be only one active Special Offer Pricelist '
                    'by company!',
                    })

    def _get_product_price(self, version_ids, product_id, quantity, args,
            price_type):
        price = False
        if version_ids.get('special') and \
                price_type not in ['normal', 'standard']:
            price = self._compute_price(version_ids['special'], product_id,
                    quantity, args, price_type=price_type)
        if not price and price_type not in ['standard'] and \
                version_ids.get('default'):
            price = self._compute_price(version_ids['default'], product_id,
                    quantity, args, price_type=price_type)
        if not price and price_type not in ['special'] and \
                version_ids.get('standard'):
            price = self._compute_price(version_ids['standard'], product_id,
                    quantity, args, price_type=price_type)
        return price

    def get_price(self, ids, price_type='default', quantity=1):
        '''
        Return the price for product ids.

        :param ids: the product ids
        :param quantity: the quantity of the products
        :param price_type: 'normal': price without specialoffers
                           'special': price only for specialoffers
                           'standard': price from standardpricelist
                           'default': default price computation
        :param Transaction().context: the context that can have as keys:
            currency: the currency id for the returned price
            pricelist: the pricelist id
            uom: the unit of measure for the product
            supplier: the supplier of the product (needed?)
            recursion: the number of recursions
        :return: a dictionary with the computed price for each product id
        '''
        with Transaction().set_context(price_type=price_type):
            res = super(Pricelist, self).get_price(ids, price_type, quantity)

        return res

    def _check_standard_pricelist(self, ids):
        pricelist = self.browse(ids)[0]
        args = [('type', '=', pricelist.type),
                ('company', '=', pricelist.company)]
        list_ids = self.search(args)
        count = 0
        for list in self.browse(list_ids):
            if list.standard==True:
                count += 1
                if count > 1:
                    return False
        args = [('type', '=', 'special'),
                ('company', '=', pricelist.company)]
        list_ids = self.search(args)
        if len(list_ids)>1:
            return False
        return True

Pricelist()


class PricelistVersion(ModelSQL, ModelView):
    _name = "pricelist.version"

    sequence = fields.Integer('Sequence', required=True)
    special = fields.Boolean('Special Offer')
    date_end = fields.Date('End Date', states={
                'readonly': Not(Bool(Eval('active'))),
                'required': Or(
                    Bool(Eval('special')),
                    Equal(
                        Get(Eval('_parent_pricelist', {}), 'type'),
                        'special'))}, depends=['active', 'special'])

    def __init__(self):
        super(PricelistVersion, self).__init__()
        self._order[0] = ('sequence', 'ASC')
        self._error_messages['date_overlap'] = ('There is only one ' +
                                      'pricelist version allowed ' +
                                      'that is not set as special offer!')

    def default_special(self):
        return False

    def default_sequence(self):
        return 10

    def _check_date(self, ids):
        for pricelist_version in self.browse(ids):
            if not pricelist_version.active:
                continue
            if not pricelist_version.date_start:
                date_start = '0001-01-01'
            else:
                date_start = pricelist_version.date_start
            if not pricelist_version.date_end:
                date_end = '0001-01-01'
            else:
                date_end = pricelist_version.date_end
            cursor = Transaction().cursor
            cursor.execute('SELECT id ' \
                    'FROM pricelist_version ' \
                    'WHERE ((date_start <= %s AND %s <= date_end ' \
                            'AND date_end IS NOT NULL) ' \
                        'OR (date_end IS NULL AND date_start IS NOT NULL ' \
                            'AND date_start <= %s) ' \
                        'OR (date_start IS NULL AND date_end IS NOT NULL ' \
                            'AND %s <= date_end) ' \
                        'OR (date_start IS NULL AND date_end IS NULL) ' \
                        'OR (%s = \'0001-01-01\' AND date_start IS NULL) ' \
                        'OR (%s = \'0001-01-01\' AND date_end IS NULL) ' \
                        'OR (%s = \'0001-01-01\' AND %s = \'0001-01-01\') ' \
                        'OR (%s = \'0001-01-01\' AND date_start <= %s) ' \
                        'OR (%s = \'0001-01-01\' AND %s <= date_end)) ' \
                        'AND pricelist = %s ' \
                        'AND active ' \
                        'AND special = FALSE ',
                           [str(date_end),
                            str(date_start),
                            str(date_end),
                            str(date_start),
                            str(date_start),
                            str(date_end),
                            str(date_start),
                            str(date_end),
                            str(date_start),
                            str(date_end),
                            str(date_end),
                            str(date_start),
                            pricelist_version.pricelist.id,
                            ])
            if len(cursor.fetchall())>1:
                return False
        return True

    def get_version_default(self, pricelist_id):
        date_obj = Pool().get('ir.date')

        date = context.get(
                'price_date', str(date_obj.today())
                )
        cursor = Transaction().cursor
        cursor.execute('SELECT id ' \
                    'FROM pricelist_version ' \
                    'WHERE pricelist = %s AND active=True ' \
                        'AND (date_start IS NULL OR date_start <= %s) ' \
                        'AND (date_end IS NULL OR date_end >= %s) ' \
                        'AND special is FALSE ' \
                    'ORDER BY id LIMIT 1', [pricelist_id, date, date])
        version = cursor.dictfetchone()
        if not version:
            return False
        return version['id']

    def get_version_special(self, pricelist_id):
        date_obj = Pool().get('ir.date')

        date = Transaction().context.get('price_date', str(date_obj.today()))
        cursor = Transaction().cursor
        cursor.execute('SELECT id ' \
                    'FROM pricelist_version ' \
                    'WHERE pricelist = %s AND active=True ' \
                        'AND (date_start IS NULL OR date_start <= %s) ' \
                        'AND (date_end IS NULL OR date_end >= %s) ' \
                        'AND special is TRUE ' \
                    'ORDER BY id LIMIT 1', [pricelist_id, date, date])
        version = cursor.dictfetchone()
        if not version:
            return self.get_version_default(pricelist_id)
        return version['id']

    def get_version(self, pl_id):
        date_obj = Pool().get('ir.date')

        date = Transaction().context.get('price_date', str(date_obj.today()))
        cursor = Transaction().cursor
        cursor.execute('SELECT id ' \
                    'FROM pricelist_version ' \
                    'WHERE pricelist = %s AND active=True ' \
                        'AND (date_start IS NULL OR date_start <= %s) ' \
                        'AND (date_end IS NULL OR date_end >= %s) ' \
                    'ORDER BY sequence', [pl_id, date, date])
        pricelist_versions = cursor.fetchall()
        version_ids = [x[0] for x in pricelist_versions]
        return version_ids

    def get_versions(self, pricelist_id):
        pricelist_obj = Pool().get('pricelist.pricelist')

        version_ids = super(PricelistVersion, self).get_versions(pricelist_id)

        #Detect Special Offer Pricelist
        pricelist = pricelist_obj.browse(pricelist_id)
        if not pricelist.type=='purchase':
            args = [('type', '=', 'special'),
                    ('company', '=', pricelist.company.id)]
            special_pricelist_id = pricelist_obj.search(args, limit=1)
            if special_pricelist_id:
                special_pricelist = pricelist_obj.browse(
                        special_pricelist_id[0])
                version_ids['special'] = self.get_version(
                        special_pricelist_id[0])

        return version_ids

PricelistVersion()


class CopyPricelistVersion(Wizard):
    'Copy Pricelist Version'
    _name = 'pricelist.pricelist.copy_pricelist_version'

    states = {
        'init': {
            'result': {
                'type': 'form',
                'object': 'pricelist.pricelist.copy_pricelist_version.init',
                'state': [
                    ('end', 'Cancel', 'tryton-cancel'),
                    ('copy', 'OK', 'tryton-ok', True),
                ],
            },
        },
        'copy': {
            'result': {
                'type': 'action',
                'action': '_copy',
                'state': 'end',
            },
        },
    }

    def _copy(self, datas):
        pool = Pool()
        pricelist_obj = pool.get('pricelist.pricelist')
        pricelist_version_obj = pool.get('pricelist.version')
        pricelist_item_obj = pool.get('pricelist.item')
        block_price_obj = pool.get('pricelist.item.block_price')

        for pricelist_id in datas['ids']:
            pricelist_version_id = pricelist_version_obj.get_version_default(
                    pricelist_id)
            if not pricelist_version_id:
                continue

            data = {}
            ids = []
            data['date_end'] = (datas['form']['date'] -
                                datetime.timedelta(days=1))

            pricelist_version_obj.write(pricelist_version_id, data)
            pricelist_version = pricelist_version_obj.browse(
                    pricelist_version_id)
            data = {}
            data['date_start'] = datas['form']['date']
            data['name'] = pricelist_version.name + ' (copy)'
            data['pricelist'] = pricelist_id
            data['special'] = pricelist_version.special
            if pricelist_version.special==True:
                data['date_end'] = datas['form']['date']

            new_id = pricelist_version_obj.create(data)
            ids.append(new_id)

            args = [('pricelist_version', '=', pricelist_version_id)]
            item_ids = pricelist_item_obj.search(args)
            items = pricelist_item_obj.browse(item_ids)
            for item in items:
                data = {}
                data['type'] = item.type
                data['product_category'] = item.product_category
                data['product'] = item.product
                data['product_template'] = item.product_template
                data['sequence'] = item.sequence
                data['markup'] = item.markup
                data['amount_fix'] = item.amount_fix
                data['pricelist_version'] = new_id
                data['pricelist'] = item.pricelist
                new_item_id = pricelist_item_obj.create(data)
                args = [('pricelist_item', '=', item.id)]
                block_price_ids = block_price_obj.search(args)
                if block_price_ids:
                    block_prices = block_price_obj.browse(block_price_ids)
                    for block_price in block_prices:
                        data = {}
                        data['quantity'] = block_price.quantity
                        data['unit_price'] = block_price.unit_price
                        data['pricelist_item'] = new_item_id
                        new_bp_id = block_price_obj.create(data)
        return ids

CopyPricelistVersion()


class PricelistReport(CompanyReport):
    _name = 'pricelist.pricelist'

    def get_version(self, pricelist_id):
        version_obj = Pool().get('pricelist.version')
        return version_obj.get_version_special(pricelist_id)

PricelistReport()
